package org.adoptopenjdk.betterrev.update.mentor_notification;

import static org.adoptopenjdk.betterrev.models.ContributionEventType.MENTOR_NOTIFIED;

import org.adoptopenjdk.betterrev.models.ContributionEvent;
import org.adoptopenjdk.betterrev.models.ContributionEventType;
import org.adoptopenjdk.betterrev.update.BetterrevActor;

/**
 * MentorNotificationActor
 */
public class MentorNotificationActor extends BetterrevActor {

    private EmailHelper emailHelper = new EmailHelper();

    @Override
    public void onReceive(Object message) throws Exception {
        if (!(message instanceof ContributionEvent)) {
            return;
        }
        ContributionEvent request = (ContributionEvent) message;
        if (request.contributionEventType != ContributionEventType.OCA_SIGNED) {
            return;
        }

        emailHelper.sendEmailMessage(request);
        publishMentorNotifiedEvent(request);
    }

    private void publishMentorNotifiedEvent(ContributionEvent request) {
        ContributionEvent mentorNotifiedEvent = new ContributionEvent(MENTOR_NOTIFIED, request.contribution);
        eventStream().publish(mentorNotifiedEvent);
    }

}
