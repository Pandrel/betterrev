package org.adoptopenjdk.betterrev;

import static com.gs.collections.impl.utility.MapIterate.forEachKeyValue;

import java.time.Duration;

import org.adoptopenjdk.betterrev.events.BetterrevEvent;
import org.adoptopenjdk.betterrev.models.ContributionEvent;
import org.adoptopenjdk.betterrev.update.BetterrevActor;
import org.adoptopenjdk.betterrev.update.bitbucket.BitbucketPoller;
import org.adoptopenjdk.betterrev.update.bitbucket.PollBitbucketEvent;
import org.adoptopenjdk.betterrev.update.mercurial.WebrevGenerator;
import org.adoptopenjdk.betterrev.update.ocachecker.OCASignedChecker;
import org.adoptopenjdk.betterrev.update.ocachecker.OCASignedEvent;
import org.adoptopenjdk.betterrev.update.pullrequest.ImportPullRequestsEvent;
import org.adoptopenjdk.betterrev.update.pullrequest.PullRequestImporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.actor.ActorRef;
import akka.actor.Props;

import com.gs.collections.api.block.procedure.Procedure2;
import com.gs.collections.impl.map.mutable.UnifiedMap;

public class Global { 
	
    private final static Logger LOGGER = LoggerFactory.getLogger(Global.class);
    
    private final static UnifiedMap<Class<? extends BetterrevActor>, Class<? extends BetterrevEvent>> actorToEventMap =
            UnifiedMap.newWithKeysValues(
                    BitbucketPoller.class, PollBitbucketEvent.class,
                    PullRequestImporter.class, ImportPullRequestsEvent.class,
                    WebrevGenerator.class, ContributionEvent.class,
                    OCASignedChecker.class, OCASignedEvent.class
            );

    /** TODO Convert this to startup servlet */
    /*
    public void onStart(Application app) {
        super.onStart(app);
        if (!app.isTest()) {
            subscribeActorsToEvents();
            regularlyPollBitbucket();
        }
    }
    */

    @SuppressWarnings("serial")
    private static void subscribeActorsToEvents() {
        forEachKeyValue(actorToEventMap, new Procedure2<Class<? extends BetterrevActor>, Class<?>>() {
            @Override
            public void value(Class<? extends BetterrevActor> actorClass, Class<?> eventClass) {
                // TODO Hook into Akka system
                // ActorRef actor = actorOf(actorClass);
                // subscribeActorToEvent(actor, eventClass);
            }
        });
    }

    /** TODO replace Play's Akka system initialisation with a straight Java EE one */
    /*
    private static ActorRef actorOf(Class<? extends BetterrevActor> actorClass) {
        return Akka.system().actorOf(Props.create(actorClass)); 
    }

    private static boolean subscribeActorToEvent(ActorRef actor, Class<?> eventClass) {
        return Akka.system().eventStream().subscribe(actor, eventClass);
    }
    */

    /** TODO replace Play's Akka system initialisation with a straight Java EE one */
    private static void regularlyPollBitbucket() {
        // TODO Get from a properties file
        // long seconds = Play.application().configuration().getLong("bitbucket.polling.duration.seconds");
        /*
        Duration pollingDuration = Duration.ofSeconds(seconds, 0);
        Akka.system().scheduler().schedule(Duration.ZERO, pollingDuration, new Runnable() {
            @Override
            public void run() {
                LOGGER.debug("Publishing new PollBitbucketEvent message.");
                Akka.system().eventStream().publish(new PollBitbucketEvent());
            }
        }, Akka.system().dispatcher());
        */
    }
    
}
