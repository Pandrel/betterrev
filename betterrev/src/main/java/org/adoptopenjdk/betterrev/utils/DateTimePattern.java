package org.adoptopenjdk.betterrev.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * DateTimeUtil class exposes methods to format Java 8 LocalDateTime to different time formats.
 * 
 * TODO May no longer be needed since we've moved to Java 8
 */
//public enum DateTimePattern {
//    
//    DEFAULT {
//        @Override
//        protected String getFormatString() {
//            return "dd MMMM yyyy kk:mm:ss";
//        }
//    };
//    
//    protected abstract String getFormatString();
//    
//    private final DateTimeFormatter dateTimeFormatter;
//    
//    private DateTimePattern() {
//        dateTimeFormatter = DateTimeFormatter.ofPattern(getFormatString());
//    }
//
//    /**
//     * Method accepts DateTime value of type org.joda.time.DateTime and produce formatted output from Pattern.DEFAULT
//     *
//     * @param dateTime of type org.joda.time.DateTime
//     * @return String
//     * @throws NullPointerException
//     */
//    public static String format(LocalDateTime dateTime) throws NullPointerException {        
//        return DEFAULT.formatDateTime(dateTime);
//    }
//
//    /**
//     * Method accepts DateTime value of type org.joda.time.DateTime and produce formatted output from given Pattern
//     * @see utils.Pattern
//     *
//     * @param dateTime of type org.joda.time.DateTime
//     * @param pattern of type util.Pattern
//     * @return String
//     * @throws NullPointerException
//     */
//    public String formatDateTime(LocalDateTime dateTime) throws NullPointerException {
//        if(dateTime == null){
//            throw new NullPointerException("Invalid parameter.DateTime could not be null");
//        }
//        
//        // TODO Fix
//        // return dateTimeFormatter.formatTo(dateTime, null);
//        return null;
//    }
//    
//}
