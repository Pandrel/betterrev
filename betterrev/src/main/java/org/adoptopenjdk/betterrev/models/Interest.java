package org.adoptopenjdk.betterrev.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Entity that represents a concept of Interest within Betterrev.
 */
@Entity
public class Interest {

    // TODO get by ID the Java EE Way
    // public static Finder<Long, Interest> find = new Finder<>(Long.class, Interest.class);

    @GeneratedValue(strategy = IDENTITY)
    @Id
    public Long id;

    public String path;

    public String project;

    public Interest() {}
    
    public Interest(String path, String project) {
        this.path = path;
        this.project = project;
    }

    @Override
    public String toString() {
        return "Interest [id=" + id + ", path=" + path + ", project=" + project + "]";
    }

    public boolean caresAbout(String repository, Set<String> filePaths) {
        for (String filePath : filePaths) {
            if (repository.equals(project) && filePath.matches(this.path)) {
                return true;
            }
        }
        return false;
    }

    public Long getKey() {
        return id;
    }

    public void setKey(Long key) {
        id = key;
    }

}