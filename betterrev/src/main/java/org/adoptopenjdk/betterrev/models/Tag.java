package org.adoptopenjdk.betterrev.models;

import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Tag entity which represents a classification for a Contribution, and can be
 * used to group Contributions (for example by Hack Day).
 */
@Entity
@Table(name="Tag")
@NamedQueries({
    @NamedQuery(name = "Tag.findAll", query = "SELECT tag FROM Tag tag")
})
public class Tag {

    // Strategy AUTO means that the JPA provider will try to pick the best strategy
    @GeneratedValue(strategy = AUTO)
    @Id
    private Long id;

    @NotNull
    private String name;

    private String description;

    public Tag(String name) {
        this.name = name;
    }

    public Tag() {}
    
    public Tag(String name, String description) {
        this.name = name;
        this.description = description;
    }
   
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }
    
}
