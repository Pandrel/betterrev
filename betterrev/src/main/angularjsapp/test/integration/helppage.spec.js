var chai = require('chai');
var expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('Help Page Tests', function() {

    // Look for the id of "helpHeading"
    it('Help Page exists', function() {
        browser.get('http://127.0.0.1:8090/#/help');
        expect(element(protractor.By.id("helpHeading")).isDisplayed()).to.eventually.equal(true);
    });

});
